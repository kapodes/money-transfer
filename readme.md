#Money Transfer Rest Service

Implemented as microservice using Java8, Netty, Jackson

*This project uses Lombok annotations so if you need Lombok plugin for IJ Idea installed 
and Annotation processing enabled in Settings -> Build -> Compiler -> Annotation Processors* 

To get runnable file use maven goal "package".

To run use `java -jar ./target/money-transfer-jar-with-dependencies.jar port` where port stands for http port for service to run.
If port is not provided or isn't a number 8080 is used.

*Service supports 3 requests:*

1. GET /info/id where id is long id value. This request returns json form of AccountInfo object.
2. GET /amount/id where id is long id value. This request returns json form of AccountAmount object.
3. POST /transfer with json body (ex `{"idFrom": 1, "idTo":2, "amount":570.23}`)

*Service returns 4 kinds of Answers*

1. 200 OK with json body `{"responce": "OK", "details": "ok"}`
2. 404 Not Found if there is no mapping for request or there is no data with provided id.
3. 400 Bad Request with json body `{"responce":"ERROR/NOT_ENOUGH_AMOUNT", "details":"Json form of request"}`
4. 400 Bad Request with json body `{"responce":"EXCEPTION", "details":"Exception message"}`

At the start of this service internal db's are populated with mock data for 4 accounts;

Account 1 and 2 with RUR currency and 100 000 amount

Account 10 and 20 with USD currency and 1000 amount

Service supports currency exchange, but only with RUR and USD. Also this functions can be extracted to separate microservice.
Same goes for Account info. I implemented them just to show possible use cases.

Databases are simple HashMaps but for production use I recommend:

1. for AccountInfo data any relational database or MongoDb with some memory caching like MemCached
2. for AccountAmount data Cassandra DB for it's fast performance and distributed and fault-tolerant nature.
3. for Exchange Rates in memory storage with constant updates from FOREX

AccountAmountDB has some locking support for concurrent usage during in-memory phase and for future actual db usage