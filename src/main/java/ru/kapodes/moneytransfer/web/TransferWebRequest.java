package ru.kapodes.moneytransfer.web;

import lombok.Data;

import java.math.BigDecimal;

@Data
class TransferWebRequest {
    private long idFrom;
    private long idTo;
    private BigDecimal amount;
}
