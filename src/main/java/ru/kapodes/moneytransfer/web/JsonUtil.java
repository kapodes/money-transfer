package ru.kapodes.moneytransfer.web;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

class JsonUtil {
    private static final ObjectMapper mapper = new ObjectMapper();

    static <T> T fromJson(String json, Class<T> clazz) throws IOException {
        return mapper.readValue(json, clazz);
    }

    static String toJson(Object responce) {
        try {
            return mapper.writeValueAsString(responce);
        } catch (IOException imposibru) {
            return "Unknown error";
        }
    }
}
