package ru.kapodes.moneytransfer.web;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.*;
import io.netty.handler.stream.ChunkedWriteHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.kapodes.moneytransfer.logic.service.MoneyTransferService;

import static io.netty.channel.ChannelOption.SO_BACKLOG;
import static io.netty.channel.ChannelOption.SO_KEEPALIVE;
import static io.netty.channel.ChannelOption.TCP_NODELAY;

@Slf4j
@RequiredArgsConstructor
public class HttpServer {
    private final int port;

    public void start(MoneyTransferService transferService) throws Exception {
        EventLoopGroup acceptGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup(2);

        try {
            ServerBootstrap serverPipeline = new ServerBootstrap();
            serverPipeline.group(acceptGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new HttpInitializer(transferService))
                    .option(SO_BACKLOG, 128)
                    .childOption(TCP_NODELAY, true)
                    .childOption(SO_KEEPALIVE, true);

            ChannelFuture f = serverPipeline.bind(port).sync();
            log.info("Http server successfully started on port "+ port);
            f.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            acceptGroup.shutdownGracefully();
            log.info("Shut down server");
        }
    }

    @RequiredArgsConstructor
    private static class HttpInitializer extends ChannelInitializer<SocketChannel> {
        private final MoneyTransferService transferService;

        @Override
        protected void initChannel(SocketChannel socketChannel) throws Exception {
            socketChannel.pipeline()
                    .addLast(new HttpRequestDecoder())
                    .addLast(new HttpObjectAggregator(1 * 1024))
                    .addLast(new HttpResponseEncoder())
                    .addLast(new HttpLogicHandler(transferService));
        }
    }

}
