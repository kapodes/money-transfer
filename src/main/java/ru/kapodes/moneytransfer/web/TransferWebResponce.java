package ru.kapodes.moneytransfer.web;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
class TransferWebResponce {
    private String responce;
    private String details;
}
