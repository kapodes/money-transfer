package ru.kapodes.moneytransfer.web;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.QueryStringDecoder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.kapodes.moneytransfer.logic.domain.AccountAmount;
import ru.kapodes.moneytransfer.logic.domain.AccountInfo;
import ru.kapodes.moneytransfer.logic.domain.TransferResult;
import ru.kapodes.moneytransfer.logic.service.MoneyTransferService;

import java.util.Arrays;

import static io.netty.handler.codec.http.HttpMethod.GET;
import static io.netty.handler.codec.http.HttpMethod.POST;
import static java.lang.Long.parseLong;
import static java.nio.charset.StandardCharsets.UTF_8;
import static ru.kapodes.moneytransfer.logic.domain.TransferResult.ERROR;
import static ru.kapodes.moneytransfer.logic.domain.TransferResult.NOT_ENOUNG_AMOUNT;
import static ru.kapodes.moneytransfer.logic.domain.TransferResult.OK;
import static ru.kapodes.moneytransfer.web.HttpUtil.badRequest;
import static ru.kapodes.moneytransfer.web.HttpUtil.notFound;
import static ru.kapodes.moneytransfer.web.HttpUtil.ok;
import static ru.kapodes.moneytransfer.web.JsonUtil.fromJson;
import static ru.kapodes.moneytransfer.web.JsonUtil.toJson;

@Slf4j
@RequiredArgsConstructor
public class HttpLogicHandler extends SimpleChannelInboundHandler<Object> {
    private final MoneyTransferService transferService;

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object o) throws Exception {
        if (o instanceof FullHttpRequest) {
            FullHttpResponse response = handleHttpMessage((FullHttpRequest) o);
            log.trace("Received request", o);
            sendAnswer(ctx, response);
        }
    }

    private FullHttpResponse handleHttpMessage(FullHttpRequest request) {
        try {
            QueryStringDecoder urlDecoder = new QueryStringDecoder(request.uri());
            if (request.method() == POST && urlDecoder.path().equals("/transfer")) {
                TransferWebRequest webRequest = fromJson(request.content().toString(UTF_8), TransferWebRequest.class);
                return processRequest(webRequest);
            } else if (request.method() == GET && urlDecoder.path().startsWith("/info/")) {
                return processInfo(urlDecoder.path());
            } else if (request.method() == GET && urlDecoder.path().startsWith("/amount/")) {
                return processAmount(urlDecoder.path());
            } else {
                log.debug("Not found request path: " + request.uri());
                return notFound();
            }
        } catch (Exception e) {
            log.error("Error processing request: ", request, e);
            return badRequest(toJson(new TransferWebResponce("EXCEPTION", e.getMessage())));
        }
    }

    private FullHttpResponse processRequest(TransferWebRequest webRequest) {
        TransferResult result = transferService.transfer(webRequest.getIdFrom(), webRequest.getIdTo(), webRequest.getAmount());
        log.trace("Transfer result: ", result);

        if (result == OK) {
            return ok(toJson(new TransferWebResponce(OK.toString(), "ok")));
        } else if (result == NOT_ENOUNG_AMOUNT) {
            return badRequest(toJson(new TransferWebResponce(NOT_ENOUNG_AMOUNT.toString(), toJson(webRequest))));
        } else {
            return badRequest(toJson(new TransferWebResponce(ERROR.toString(), toJson(webRequest))));
        }
    }

    private FullHttpResponse processAmount(String path) {
        Long id = parseIdFromPath(path);
        if (id == null) return badRequest(path);
        AccountAmount amount = transferService.getAmount(id);
        return amount == null ? notFound() : ok(toJson(amount));
    }

    private FullHttpResponse processInfo(String path) {
        Long id = parseIdFromPath(path);
        if (id == null) return badRequest(path);
        AccountInfo info = transferService.getInfo(id);
        return info == null ? notFound() : ok(toJson(info));
    }

    private Long parseIdFromPath(String path) {
        String[] pathVars = path.split("/");
        if (pathVars.length < 3) return null;
        try {
            return parseLong(pathVars[2]);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private void sendAnswer(ChannelHandlerContext ctx, FullHttpResponse answer) {
        ctx.channel().writeAndFlush(answer);
        log.trace("Sent answer: " + answer);
    }

}
