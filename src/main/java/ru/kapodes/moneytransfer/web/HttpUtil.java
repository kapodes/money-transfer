package ru.kapodes.moneytransfer.web;

import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;

import static io.netty.buffer.Unpooled.copiedBuffer;
import static io.netty.handler.codec.http.HttpHeaderNames.CONNECTION;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_LENGTH;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpHeaderNames.HOST;
import static io.netty.handler.codec.http.HttpResponseStatus.BAD_REQUEST;
import static io.netty.handler.codec.http.HttpResponseStatus.NOT_FOUND;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;
import static java.nio.charset.StandardCharsets.UTF_8;

class HttpUtil {
    private static final String APP_JSON = "application/json";

    static FullHttpResponse ok(String body) {
        FullHttpResponse response = response(OK);
        return insertBody(response, body);
    }

    static FullHttpResponse badRequest(String body) {
        FullHttpResponse response = response(BAD_REQUEST);
        return insertBody(response, body);
    }

    static FullHttpResponse notFound() {
        FullHttpResponse response = response(NOT_FOUND);
        response.headers().set(CONTENT_LENGTH, 0);
        return response;
    }

    private static FullHttpResponse response(HttpResponseStatus status) {
        DefaultFullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, status);
        response.headers().set(HOST, "localhost");
        response.headers().set(CONNECTION, "keep-alive");
        return response;
    }

    private static FullHttpResponse insertBody(FullHttpResponse response, String body) {
        ByteBuf buf = copiedBuffer(body, UTF_8);
        response.headers().set(CONTENT_TYPE, APP_JSON);
        response.headers().set(CONTENT_LENGTH, buf.readableBytes());
        response.content().clear().writeBytes(buf);
        buf.release();
        return response;
    }
}
