package ru.kapodes.moneytransfer.logic.db;

import ru.kapodes.moneytransfer.logic.domain.AccountInfo;

public interface AccountInfoDB {
    AccountInfo getInfo(long id);

    void addNew(String currencyCode);
}
