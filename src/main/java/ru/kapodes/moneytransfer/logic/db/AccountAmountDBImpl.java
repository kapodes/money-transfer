package ru.kapodes.moneytransfer.logic.db;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import ru.kapodes.moneytransfer.logic.domain.AccountAmount;
import ru.kapodes.moneytransfer.logic.domain.TransferRequest;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.Thread.currentThread;
import static java.util.Collections.synchronizedSet;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AccountAmountDBImpl implements AccountAmountDB {
    private static final AccountAmountDBImpl instance = new AccountAmountDBImpl();
    private final Map<Long, AccountAmount> amountDB = new ConcurrentHashMap<>();

    private final Lock updateLock = new ReentrantLock();
    private final Condition lockedAccountCondition = updateLock.newCondition();

    private final List<UpdateLock> lockedIds = new CopyOnWriteArrayList<>();

    public static AccountAmountDBImpl getInstance() {
        return instance;
    }

    @Override
    public void create(AccountAmount newAmount) {
        amountDB.merge(newAmount.getId(), newAmount, AccountAmountDBImpl::merge);
    }

    @Override
    public AccountAmount read(long id) {
        return amountDB.get(id);
    } //doesn't need locking for simple read

    @Override
    public void update(AccountAmount update, TransferRequest request) throws NotLockedAccountException {
        checkLock(update.getId(), request);
        amountDB.put(update.getId(), update);
    }

    private void checkLock(Long id, TransferRequest request) throws NotLockedAccountException {
        UpdateLock requestedId = new UpdateLock(id, request);
        if (lockedIds.stream().filter(lockedIds -> lockedIds.fullEquals(requestedId)).count() == 0) {
            throw new NotLockedAccountException();
        }
    }

    private static AccountAmount merge(AccountAmount oldAmount, AccountAmount newAmount) {
        throw new RuntimeException("Adding existing account amount not supported: " + newAmount.getId());
    }

    @Override
    public void delete(long id) {
        amountDB.remove(id);
    }

    @Override
    public void lockUpdate(long id, TransferRequest request) {
        updateLock.lock();
        UpdateLock newId = new UpdateLock(id, request);
        while (lockedIds.contains(newId)) {
            try {
                lockedAccountCondition.await(1, MILLISECONDS);
            } catch (InterruptedException e) {
                currentThread().interrupt();
            }
        }

        lockedIds.add(newId);
        updateLock.unlock();
    }

    @Override
    public void unlockUpdate(long id, TransferRequest request) {
        updateLock.lock();
        lockedIds.remove(new UpdateLock(id, request));
        lockedAccountCondition.signal();
        updateLock.unlock();
    }

    public void initMockData() {
        amountDB.put(1L, new AccountAmount(1L, new BigDecimal(100000)));
        amountDB.put(2L, new AccountAmount(2L, new BigDecimal(100000)));
        amountDB.put(10L, new AccountAmount(10L, new BigDecimal(1000)));
        amountDB.put(20L, new AccountAmount(20L, new BigDecimal(1000)));
    }

    @RequiredArgsConstructor
    //can have a small cache for this for memory optimization
    private static class UpdateLock {
        private final long id;
        private final TransferRequest request;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            UpdateLock lock = (UpdateLock) o;

            return id == lock.id;
        }

        @Override
        public int hashCode() {
            return (int) (id ^ (id >>> 32));
        }

        private boolean fullEquals(UpdateLock otherLock) {
            return id == otherLock.id && request.equals(otherLock.request);
        }
    }
}
