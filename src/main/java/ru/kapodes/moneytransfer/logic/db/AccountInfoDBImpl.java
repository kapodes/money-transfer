package ru.kapodes.moneytransfer.logic.db;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ru.kapodes.moneytransfer.logic.domain.AccountInfo;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AccountInfoDBImpl implements AccountInfoDB {
    private static final AccountInfoDBImpl instance = new AccountInfoDBImpl();

    private AtomicLong idGen = new AtomicLong(0);
    private final Map<Long, AccountInfo> infoDB = new ConcurrentHashMap<>();

    public static AccountInfoDBImpl getInstance() {
        return instance;
    }

    @Override
    public AccountInfo getInfo(long id) {
        return infoDB.get(id);
    }

    @Override
    public void addNew(String currencyCode) {
        Long newId = generateId();
        AccountInfo newInfo = new AccountInfo(newId, currencyCode);
        infoDB.put(newId, newInfo);
    }

    private Long generateId() {
        return idGen.incrementAndGet();
    }

    public void initMockData() {
        infoDB.put(1L, new AccountInfo(1L, "RUR"));
        infoDB.put(2L, new AccountInfo(2L, "RUR"));
        infoDB.put(10L, new AccountInfo(10L, "USD"));
        infoDB.put(20L, new AccountInfo(20L, "USD"));
    }
}
