package ru.kapodes.moneytransfer.logic.db;

import ru.kapodes.moneytransfer.logic.domain.AccountAmount;
import ru.kapodes.moneytransfer.logic.domain.TransferRequest;

public interface AccountAmountDB {

    void create(AccountAmount newAmount);

    AccountAmount read(long id);

    void update(AccountAmount update, TransferRequest request) throws NotLockedAccountException;

    void delete(long id);

    void lockUpdate(long id, TransferRequest request);

    void unlockUpdate(long id, TransferRequest request);
}
