package ru.kapodes.moneytransfer.logic.transfer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.kapodes.moneytransfer.logic.db.AccountAmountDB;
import ru.kapodes.moneytransfer.logic.db.NotLockedAccountException;
import ru.kapodes.moneytransfer.logic.domain.AccountAmount;
import ru.kapodes.moneytransfer.logic.domain.TransferRequest;
import ru.kapodes.moneytransfer.logic.domain.TransferResult;

import static java.lang.Long.max;
import static java.lang.Long.min;
import static ru.kapodes.moneytransfer.logic.domain.TransferResult.ERROR;
import static ru.kapodes.moneytransfer.logic.domain.TransferResult.NOT_ENOUNG_AMOUNT;
import static ru.kapodes.moneytransfer.logic.domain.TransferResult.OK;

@Slf4j
@RequiredArgsConstructor
public class TransferServiceImpl implements TransferService {
    private final AccountAmountDB amountDB;

    @Override
    public TransferResult transfer(TransferRequest request) {
        try {

            amountDB.lockUpdate(min(request.getAccountFrom().getId(), request.getAccountTo().getId()), request);
            amountDB.lockUpdate(max(request.getAccountFrom().getId(), request.getAccountTo().getId()), request);

            AccountAmount accFrom = amountDB.read(request.getAccountFrom().getId());

            if (notEnough(accFrom, request)) {
                log.warn("Not enough balance on account " + accFrom + " requested: " + request.getFromAmount());
                return NOT_ENOUNG_AMOUNT;
            }

            AccountAmount accTo = amountDB.read(request.getAccountTo().getId());
            TransferResult transferResult = transfer(accFrom, accTo, request);
            return transferResult;
        } catch (Exception e) {
            log.error("Exception during transfer ", request, e);
            return ERROR;
        } finally {
            amountDB.unlockUpdate(request.getAccountFrom().getId(), request);
            amountDB.unlockUpdate(request.getAccountTo().getId(), request);
        }
    }

    private static boolean notEnough(AccountAmount accountAmount, TransferRequest request) {
        return accountAmount == null || accountAmount.getAmount().compareTo(request.getFromAmount()) < 0;
    }

    private TransferResult transfer(AccountAmount from, AccountAmount to, TransferRequest request) throws NotLockedAccountException {
        AccountAmount newFrom = new AccountAmount(from.getId(), from.getAmount().subtract(request.getFromAmount()));
        AccountAmount newTo = new AccountAmount(to.getId(), to.getAmount().add(request.getToAmount()));

        try {
            amountDB.update(newFrom, request);
            amountDB.update(newTo, request);
        } catch (RuntimeException e) {
            log.error("Error during transfer transaction {}, reventing", request, e);
            amountDB.update(from, request);
            amountDB.update(to, request); //simple implementation for this test
            return ERROR;
        }

        log.trace("Completed request ", request);
        return OK;
    }
}
