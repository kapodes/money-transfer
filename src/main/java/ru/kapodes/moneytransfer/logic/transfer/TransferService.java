package ru.kapodes.moneytransfer.logic.transfer;

import ru.kapodes.moneytransfer.logic.domain.TransferRequest;
import ru.kapodes.moneytransfer.logic.domain.TransferResult;

public interface TransferService {
    TransferResult transfer(TransferRequest request);
}
