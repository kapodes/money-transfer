package ru.kapodes.moneytransfer.logic.conversion;

import java.math.BigDecimal;

public interface CurrencyConversionService {
    BigDecimal coversionRate(String currencyCodeFrom, String currencyCodeTo) throws CurrencyNotSupported;
}
