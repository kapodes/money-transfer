package ru.kapodes.moneytransfer.logic.conversion;

public class CurrencyNotSupported extends Exception {

    public CurrencyNotSupported(String conversion) {
        super("Currency conversion not supported: " + conversion);
    }
}
