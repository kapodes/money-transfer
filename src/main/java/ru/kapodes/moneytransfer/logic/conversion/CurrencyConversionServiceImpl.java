package ru.kapodes.moneytransfer.logic.conversion;

import java.math.BigDecimal;

public class CurrencyConversionServiceImpl implements CurrencyConversionService {

    @Override
    //very simple for now :)
    public BigDecimal coversionRate(String currencyCodeFrom, String currencyCodeTo) throws CurrencyNotSupported {
        if (currencyCodeFrom.equals(currencyCodeTo)) return BigDecimal.ONE;
        String conversion = currencyCodeFrom + "-" + currencyCodeTo;

        switch (conversion) {
            case "USD-RUR":
                return BigDecimal.valueOf(57);
            case "RUR-USD":
                return BigDecimal.valueOf(0.01754d);
            default:
                throw new CurrencyNotSupported(conversion);
        }
    }
}
