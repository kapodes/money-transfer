package ru.kapodes.moneytransfer.logic.prepare;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.kapodes.moneytransfer.logic.conversion.CurrencyConversionService;
import ru.kapodes.moneytransfer.logic.conversion.CurrencyNotSupported;
import ru.kapodes.moneytransfer.logic.domain.AccountInfo;
import ru.kapodes.moneytransfer.logic.domain.TransferRequest;
import ru.kapodes.moneytransfer.logic.info.AccountInfoService;

import java.math.BigDecimal;

import static java.math.BigDecimal.ROUND_FLOOR;

@Slf4j
@RequiredArgsConstructor
public class PrepareTransferServiceImpl implements PrepareTransferService {
    private final CurrencyConversionService conversionService;
    private final AccountInfoService infoService;

    @Override
    public TransferRequest prepare(long idFrom, long idTo, BigDecimal amount) {
        AccountInfo from = infoService.getAccountInfo(idFrom);
        AccountInfo to = infoService.getAccountInfo(idTo);
        BigDecimal conversionRate = calculateConversionRate(from, to);
        BigDecimal toAmount = amount.multiply(conversionRate).setScale(2, ROUND_FLOOR);
        TransferRequest transferRequest = new TransferRequest(from, amount, to, toAmount);

        log.trace("Prepared request ", transferRequest);
        //todo can implement service for commission gathering
        //todo can implement service for multiplication scaling leftOvers gathering like 123.1234 -> 123.12 (transfer)+ 0.0034 (keep)
        return transferRequest;
    }

    private BigDecimal calculateConversionRate(AccountInfo from, AccountInfo to) {
        try {
            return conversionService.coversionRate(from.getCurrencyCode(), to.getCurrencyCode());
        } catch (CurrencyNotSupported e) {
            log.error("Currency pair not supported: ", from.getCurrencyCode(), to.getCurrencyCode());
            throw new RuntimeException(e.getMessage());
        }
    }
}
