package ru.kapodes.moneytransfer.logic.prepare;

import ru.kapodes.moneytransfer.logic.domain.TransferRequest;

import java.math.BigDecimal;

public interface PrepareTransferService {
    TransferRequest prepare(long idFrom, long idTo, BigDecimal amount);
}
