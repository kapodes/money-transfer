package ru.kapodes.moneytransfer.logic.service;

import lombok.extern.slf4j.Slf4j;
import ru.kapodes.moneytransfer.logic.conversion.CurrencyConversionServiceImpl;
import ru.kapodes.moneytransfer.logic.db.AccountAmountDBImpl;
import ru.kapodes.moneytransfer.logic.db.AccountInfoDBImpl;
import ru.kapodes.moneytransfer.logic.domain.AccountAmount;
import ru.kapodes.moneytransfer.logic.domain.AccountInfo;
import ru.kapodes.moneytransfer.logic.domain.TransferRequest;
import ru.kapodes.moneytransfer.logic.domain.TransferResult;
import ru.kapodes.moneytransfer.logic.info.AccountInfoServiceImpl;
import ru.kapodes.moneytransfer.logic.prepare.PrepareTransferService;
import ru.kapodes.moneytransfer.logic.prepare.PrepareTransferServiceImpl;
import ru.kapodes.moneytransfer.logic.transfer.TransferService;
import ru.kapodes.moneytransfer.logic.transfer.TransferServiceImpl;

import java.math.BigDecimal;

import static ru.kapodes.moneytransfer.logic.domain.TransferResult.ERROR;

//Thread-Safe. Multiple copies or multiple users allowed
@Slf4j
public class MoneyTransferService {
    private final PrepareTransferService prepareService;
    private final TransferService transferService;

    public MoneyTransferService() {
        //hello Spring :)
        AccountInfoDBImpl.getInstance().initMockData();
        AccountAmountDBImpl.getInstance().initMockData();
        prepareService = new PrepareTransferServiceImpl(new CurrencyConversionServiceImpl(),
                new AccountInfoServiceImpl(AccountInfoDBImpl.getInstance()));
        transferService = new TransferServiceImpl(AccountAmountDBImpl.getInstance());

        log.info("Initialized");
    }

    public TransferResult transfer(long idFrom, long idTo, BigDecimal amount) {
        try {
            TransferRequest transferRequest = prepareService.prepare(idFrom, idTo, amount);
            return transferService.transfer(transferRequest);
        } catch (RuntimeException e) {
            log.error("Error transfering funds", e);
            return ERROR;
        }
    }

    public AccountAmount getAmount(long id) {
        return AccountAmountDBImpl.getInstance().read(id);
    }

    public AccountInfo getInfo(long id) {
        return AccountInfoDBImpl.getInstance().getInfo(id);
    }
}
