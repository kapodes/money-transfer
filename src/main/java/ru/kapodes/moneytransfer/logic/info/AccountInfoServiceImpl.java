package ru.kapodes.moneytransfer.logic.info;

import lombok.RequiredArgsConstructor;
import ru.kapodes.moneytransfer.logic.db.AccountInfoDB;
import ru.kapodes.moneytransfer.logic.domain.AccountInfo;

@RequiredArgsConstructor
public class AccountInfoServiceImpl implements AccountInfoService {
    private final AccountInfoDB infoDB;

    @Override
    public AccountInfo getAccountInfo(long accountId) {
        return infoDB.getInfo(accountId);
    }
}
