package ru.kapodes.moneytransfer.logic.info;

import ru.kapodes.moneytransfer.logic.domain.AccountInfo;

public interface AccountInfoService {
    AccountInfo getAccountInfo(long accountId);
}
