package ru.kapodes.moneytransfer.logic.domain;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransferRequest {
    private final AccountInfo accountFrom;
    private final BigDecimal fromAmount;
    private final AccountInfo accountTo;
    private final BigDecimal toAmount;
}
