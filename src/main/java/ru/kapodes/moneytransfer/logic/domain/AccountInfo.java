package ru.kapodes.moneytransfer.logic.domain;

import lombok.Data;

@Data
public class AccountInfo {
    private final long id;
    private final String currencyCode;
}
