package ru.kapodes.moneytransfer.logic.domain;

import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(exclude = "amount", doNotUseGetters = true)
@ToString
public class AccountAmount {
    @NonNull
    private final Long id;
    private volatile BigDecimal amount;

    public static AccountAmount minId(AccountAmount first, AccountAmount second) {
        return first.id < second.id ? first : second;
    }

    public static AccountAmount maxId(AccountAmount first, AccountAmount second) {
        return first.id > second.id ? first : second;
    }
}
