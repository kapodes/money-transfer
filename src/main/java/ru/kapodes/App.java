package ru.kapodes;

import lombok.extern.slf4j.Slf4j;
import ru.kapodes.moneytransfer.logic.service.MoneyTransferService;
import ru.kapodes.moneytransfer.web.HttpServer;

@Slf4j
public class App {
    public static void main(String[] args) throws Exception {
        int port = 8080;
        if (args.length == 1) {
            try {
                port = Integer.parseInt(args[0]);
            } catch (NumberFormatException ignored) {
            }
        }

        MoneyTransferService transferService = new MoneyTransferService();
        log.info("Server starting on port: " + port);
        HttpServer httpServer = new HttpServer(port);
        httpServer.start(transferService);
    }
}
