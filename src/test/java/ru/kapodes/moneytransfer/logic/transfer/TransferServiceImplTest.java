package ru.kapodes.moneytransfer.logic.transfer;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import ru.kapodes.moneytransfer.logic.db.AccountAmountDB;
import ru.kapodes.moneytransfer.logic.db.AccountAmountDBImpl;
import ru.kapodes.moneytransfer.logic.domain.AccountAmount;
import ru.kapodes.moneytransfer.logic.domain.AccountInfo;
import ru.kapodes.moneytransfer.logic.domain.TransferRequest;
import ru.kapodes.moneytransfer.logic.domain.TransferResult;

import java.math.BigDecimal;
import java.util.concurrent.ExecutorService;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static java.util.Arrays.asList;
import static java.util.concurrent.Executors.newFixedThreadPool;
import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.stream.IntStream.range;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static ru.kapodes.moneytransfer.logic.domain.TransferResult.ERROR;
import static ru.kapodes.moneytransfer.logic.domain.TransferResult.NOT_ENOUNG_AMOUNT;
import static ru.kapodes.moneytransfer.logic.domain.TransferResult.OK;

@RunWith(MockitoJUnitRunner.class)
public class TransferServiceImplTest {
    private static AccountAmountDB amountDB;

    private TransferServiceImpl transferService;

    @BeforeClass
    public static void init() {
        amountDB = AccountAmountDBImpl.getInstance();
    }

    @Before
    public void setUp() {
        transferService = new TransferServiceImpl(amountDB);
    }

    @Test
    public void transferOkTest() {
        initAmount(1, 1);
        initAmount(2, 0);

        TransferRequest oneToTwo = new TransferRequest(info(1), ONE, info(2), ONE);
        assertEquals(OK, transferService.transfer(oneToTwo));

        assertEquals(ZERO, amountDB.read(1).getAmount());
        assertEquals(ONE, amountDB.read(2).getAmount());
    }

    @Test
    public void transferNotEnoughTest() {
        initAmount(10, 1);
        initAmount(20, 1);

        TransferResult result = transferService.transfer(new TransferRequest(info(10L), TEN, info(20L), TEN));
        assertEquals(NOT_ENOUNG_AMOUNT, result);

        TransferResult resultNoAccount = transferService.transfer(new TransferRequest(info(30L), TEN, info(20L), TEN));
        assertEquals(NOT_ENOUNG_AMOUNT, resultNoAccount);
    }

    @Test
    public void transferErrorTest() {
        AccountInfo falseInfo = Mockito.spy(info(100L));
        when(falseInfo.getId()).thenThrow(new RuntimeException()).thenReturn(100L);
        TransferResult result = transferService.transfer(new TransferRequest(falseInfo, TEN, info(200L), TEN));
        assertEquals(ERROR, result);
    }

    @Test(timeout = 3 * 1000) //just in case
    public void transferOkSyncTest() throws InterruptedException {
        initAmount(1_000L, 5_000L);
        initAmount(2_000L, 5_000L);
        initAmount(3_000L, 5_000L);
        initAmount(4_000L, 5_000L);
        initAmount(5_000L, 5_000L);

        Runnable job1 = transferJob(transferService, 1_000L, 2_000L, 5000);
        Runnable job2 = transferJob(transferService, 2_000L, 3_000L, 5000);
        Runnable job3 = transferJob(transferService, 3_000L, 4_000L, 5000);
        Runnable job4 = transferJob(transferService, 4_000L, 5_000L, 5000);

        Runnable job1Back = transferJob(transferService, 2_000L, 1_000L, 1000);
        Runnable job2Back = transferJob(transferService, 3_000L, 2_000L, 2000);
        Runnable job3Back = transferJob(transferService, 4_000L, 3_000L, 3000);
        Runnable job4Back = transferJob(transferService, 5_000L, 4_000L, 4000);

        Runnable readJob1 = () -> range(0, 10_000).forEach(i -> amountDB.read(1_000L));
        Runnable readJob2 = () -> range(0, 10_000).forEach(i -> amountDB.read(5_000L));

        ExecutorService executorService = newFixedThreadPool(10);
        asList(job1, job2, job3, job4, job1Back, job2Back, job3Back, job4Back, readJob1, readJob2).forEach(executorService::submit);

        executorService.shutdown();
        executorService.awaitTermination(2, SECONDS);

        checkAmount(1_000, new BigDecimal(1000));
        checkAmount(2_000, new BigDecimal(6000));
        checkAmount(3_000, new BigDecimal(6000));
        checkAmount(4_000, new BigDecimal(6000));
        checkAmount(5_000, new BigDecimal(6000));
    }

    private void initAmount(long id, long amount) {
        amountDB.create(new AccountAmount(id, new BigDecimal(amount)));
    }

    private Runnable transferJob(TransferService service, Long idFrom, Long idTo, int count) {
        TransferRequest request = new TransferRequest(info(idFrom), ONE, info(idTo), ONE);

        return () -> assertEquals(0,
                range(0, count)
                        .mapToObj(i -> service.transfer(request))
                        .filter(result -> result != OK)
                        .peek(System.out::println)
                        .count());
    }

    private static AccountInfo info(long id) {
        return new AccountInfo(id, "RUR");
    }

    private static void checkAmount(long id, BigDecimal amount) {
        assertEquals(amount, amountDB.read(id).getAmount());
    }

}