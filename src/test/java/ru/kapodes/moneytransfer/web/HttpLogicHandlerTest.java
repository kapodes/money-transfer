package ru.kapodes.moneytransfer.web;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.kapodes.moneytransfer.logic.domain.TransferResult;
import ru.kapodes.moneytransfer.logic.service.MoneyTransferService;

import java.math.BigDecimal;

import static io.netty.handler.codec.http.HttpMethod.GET;
import static io.netty.handler.codec.http.HttpMethod.POST;
import static io.netty.handler.codec.http.HttpResponseStatus.BAD_REQUEST;
import static io.netty.handler.codec.http.HttpResponseStatus.NOT_FOUND;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static ru.kapodes.moneytransfer.logic.domain.TransferResult.ERROR;
import static ru.kapodes.moneytransfer.logic.domain.TransferResult.NOT_ENOUNG_AMOUNT;
import static ru.kapodes.moneytransfer.logic.domain.TransferResult.OK;
import static ru.kapodes.moneytransfer.web.JsonUtil.toJson;

@RunWith(MockitoJUnitRunner.class)
public class HttpLogicHandlerTest {
    private static final String REQUEST_JSON = "{\"idFrom\":1,\"idTo\":2,\"amount\":12.234}";

    @Mock
    private MoneyTransferService transferService;

    @Mock
    private ChannelHandlerContext ctx;

    @Mock
    private Channel channel;

    @Mock
    private FullHttpRequest httpRequest;

    @Mock
    private ByteBuf buf;

    ArgumentCaptor<FullHttpResponse> responceCaptor = ArgumentCaptor.forClass(FullHttpResponse.class);

    private HttpLogicHandler handler;

    @Before
    public void setUp() {
        when(ctx.channel()).thenReturn(channel);
        when(httpRequest.content()).thenReturn(buf);
        when(channel.writeAndFlush(responceCaptor.capture())).thenReturn(null);
        handler = new HttpLogicHandler(transferService);
    }

    @Test
    public void notFoundTest() throws Exception {
        tuneRequestMethodUrl(GET, "/url");
        handler.channelRead0(ctx, httpRequest);
        verify(channel).writeAndFlush(any(FullHttpResponse.class));
        assertEquals(NOT_FOUND, responceCaptor.getValue().status());
        verifyNoMoreInteractions(transferService);
    }

    @Test
    public void testErrors() throws Exception {
        tuneRequestMethodUrl(POST, "/transfer");
        when(httpRequest.content()).thenThrow(new RuntimeException("ExMessage"));
        handler.channelRead0(ctx, httpRequest);
        FullHttpResponse response = responceCaptor.getValue();
        assertEquals(BAD_REQUEST, response.status());
        checkBody(response, toJson(new TransferWebResponce("EXCEPTION", "ExMessage")));
        verifyNoMoreInteractions(transferService);
    }

    @Test
    public void normalWayTest() throws Exception {
        tuneRequestAnswer(OK);
        handler.channelRead0(ctx, httpRequest);
        FullHttpResponse response = responceCaptor.getValue();
        assertEquals(HttpResponseStatus.OK, response.status());
        checkBody(response, "{\"responce\":\"OK\",\"details\":\"ok\"}");
    }

    @Test
    public void notEnoughWayTest() throws Exception {
        tuneRequestAnswer(NOT_ENOUNG_AMOUNT);
        handler.channelRead0(ctx, httpRequest);
        FullHttpResponse response = responceCaptor.getValue();
        assertEquals(BAD_REQUEST, response.status());
        checkBody(response, "{\"responce\":\"NOT_ENOUNG_AMOUNT\",\"details\":\"" +
                REQUEST_JSON + "\"}");
    }

   @Test
    public void errorWayTest() throws Exception {
        tuneRequestAnswer(ERROR);
        handler.channelRead0(ctx, httpRequest);
        FullHttpResponse response = responceCaptor.getValue();
        assertEquals(BAD_REQUEST, response.status());
        checkBody(response, "{\"responce\":\"ERROR\",\"details\":\"" +
                REQUEST_JSON + "\"}");
    }

    private void tuneRequestMethodUrl(HttpMethod method, String url) {
        when(httpRequest.method()).thenReturn(method);
        when(httpRequest.uri()).thenReturn(url);
    }

    private void checkBody(FullHttpResponse response, String expected) {
        assertEquals(expected, response.content().toString(UTF_8).replace("\\",""));
    }

    private void tuneRequestAnswer(TransferResult result) {
        when(buf.toString(UTF_8)).thenReturn(REQUEST_JSON);
        tuneRequestMethodUrl(POST, "/transfer");
        when(transferService.transfer(1, 2, BigDecimal.valueOf(12.234))).thenReturn(result);
    }

}